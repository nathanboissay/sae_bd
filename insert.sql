insert into zoo values (1,'Beauval','Boulay','Egypte','0632861900','Beauval@gmail.com','koko');
insert into Safaris values(100,'Voiture',1);
insert into Emplacement values(1,'Terre','Cailloux','Evacuation',1);
insert into Famille values('volatile','il est beau','binaire');
insert into Especes values('leopard','luila','volatile');
insert into animaux values(1,'Koko','Gaspard','M',to_date('20/10/2004','dd/mm/yyyy'),to_date('14/12/2020','dd/mm/yyyy'),'Tres agreable',1,'leopard');
insert into ZoneGeographique values(1,'Terre aride');
insert into Aliment values('pomme de terre');
insert into Aliment values('pomme tout court');
insert into Aliment values('pate');
insert into Aliment values('courgette');
insert into PeutEtreSubstituer values('pate','courgette',2.5);

insert into Stock values(1,'pomme de terre',4);
insert into Mange values('leopard','pomme de terre',1);
insert into Vivre values(1,'leopard',4);
insert into Appartient values('leopard','volatile',15);
insert into PeutEtreSubstituer values('pomme de terre','pomme tout court', 15.20);
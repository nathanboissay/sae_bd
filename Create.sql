@delete

Create table zoo(
    codeID Number(5),
    Nom VarChar(20),
    Ville VarChar(20),
    Pays VarChar(20),
    Telephone Number(10),
    Email VarChar(30),
    NomResponsable VarChar(20),
    constraint cleZoo primary key (codeID));

Create table Safaris(
    Superficie Number(6),
    ModeDeLocomotion VarChar(20),
    codeID Number(5),
    constraint cleSafaris primary key (codeID),
    constraint zooExiste foreign key (codeID) REFERENCES zoo(codeID));


Create table Zoologique (
    Intitulé_equipe VarChar(20),
    codeID Number(5),
    constraint cleZoologique primary key (codeID),
    constraint zooExiste2 foreign key (codeID) REFERENCES zoo(codeID));


Create table Emplacement(
    Code Number(9) ,
    Types VarChar2(20),
    SousType VarChar2(20),
    ProcedureSecurite VarChar2(100),
    codeID Number(5),
    constraint cleEmplacement primary key (Code),
    constraint zooExiste3 foreign key (codeID) REFERENCES zoo(codeID));


Create table Famille (
    NomFamille VarChar2(30),
    Descriptions VarChar2(200),
    Caracteristique VarChar2(200),
    constraint CleFamille primary key (NomFamille));

Create table Especes (
    NomScientifique VarChar2(40),
    NomVulgaire VarChar2(30),
    NomFamille VarChar2(30),
    constraint CleEspeces primary key (NomScientifique),
    constraint familleExiste foreign key (NomFamille) REFERENCES Famille (NomFamille));

Create table animaux(
    NumeroID Number(5),
    NomResponsableAnimaux VarChar2(20),
    NomIndividue VarChar2(20),
    Sexe VarChar2(1),
    DateDeNaissance date,
    DateArrive date,
    Remarque VarChar2(100),
    Code number(9),
    NomScientifique VarChar2(40),
    constraint cleAnimaux primary key (NumeroID),
    constraint emplacementExiste foreign key (Code) REFERENCES Emplacement (Code),
    constraint especesExiste foreign key (NomScientifique) REFERENCES Especes (NomScientifique));


Create table ZoneGeographique(
    codeZoo Number(5),
    Libellée VarChar2(30),
    constraint CleZoneGeographique primary key (codeZoo));


Create table Aliment(
    Nourriture VarChar2(30),
    constraint CleAliment primary key (Nourriture));

create table Stock(
    codeID number(5),
    Nourriture VarChar2(30),
    qtStocker number(4),
    constraint cleStock primary key (codeID,Nourriture),
    constraint zooExiste4 foreign key (codeID) REFERENCES zoo(codeID),
    constraint alimentExiste foreign key (Nourriture) REFERENCES Aliment(Nourriture));

create table Mange(
    NomScientifique VarChar2(40),
    Nourriture VarChar2(30),
    Quantite number(4),
    constraint cleMange primary key (NomScientifique,Nourriture),
    constraint especesExiste2 foreign key (NomScientifique) REFERENCES Especes (NomScientifique),
    constraint alimentExiste2 foreign key (Nourriture) REFERENCES Aliment (Nourriture));

create table Vivre(
    codeZoo number(5),
    NomScientifique VarChar2(40),
    populationEstimee number(9),
    constraint cleVivre primary key (codeZoo,NomScientifique),
    constraint ZoneGeographiqueExiste foreign key (codeZoo) REFERENCES ZoneGeographique(codeZoo),
    constraint especesExiste3 foreign key (NomScientifique) REFERENCES Especes(NomScientifique));

create table Appartient(
    NomScientifique VarChar2(40),
    NomFamille VarChar2(30),
    populationTotal number(9),
    constraint cleAppartient primary key (NomScientifique,NomFamille),
    constraint especesExiste4 foreign key (NomScientifique) REFERENCES Especes(NomScientifique),
    constraint familleExiste2 foreign key (NomFamille) REFERENCES Famille(NomFamille));

create table PeutEtreSubstituer(
    NourritureA VarChar2(30),
    NourritureB VarChar2(30),
    tauxSubstitution number(4,2),
    constraint clePeutEtreSubstituer primary key (NourritureA,NourritureB),
    constraint NourritureAExiste foreign key (NourritureA) REFERENCES Aliment(Nourriture),
    constraint NourritureBExiste foreign key (NourritureB) REFERENCES Aliment(Nourriture));

@insert;